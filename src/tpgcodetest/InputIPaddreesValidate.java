/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tpgcodetest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ravi
 */
public class InputIPaddreesValidate {

    public static void main(String a[]) throws FileNotFoundException, IOException {

        String inp = "";
        File file = new File("C:\\IpAdresses.txt");
        try {
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                inp += scan.nextLine() + ", ";
            }
        } catch (FileNotFoundException f) {
            f.printStackTrace();
        }


        System.out.println("File inp string is " + inp);

        Pattern pattern =
                Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
        Matcher match = pattern.matcher(inp);
        while (match.find()) {
            System.out.println("IP found: " + match.group());
        }

    }
}
